import React, {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
} from 'react';
import { isIOS, isMobile } from 'react-device-detect';
import ReactPlayer from 'react-player';
import fullScreenIcon from '../../assets/img/fullscreen.svg';
import playIcon from '../../assets/img/play_circle.svg';
import settingsIcon from '../../assets/img/settings.svg';
import syncIcon from '../../assets/img/sync.svg';
import muteIcon from '../../assets/img/volume_off.svg';
import volumeIcon from '../../assets/img/volume_up.svg';
import Spinner from '../../components/Spinner';
import dashConfig from './dashjsConfig';
import YouTube from 'react-youtube';
import { extractYoutubeId } from '../../utils';
import { logActivity, markSessionComplete } from '../../utils/sessions';
import {
  initCleverTap,
  recordCleverTapEvent,
  initCleverTapUser,
} from '../../utils/cleverTap';
import { getUserId } from '../../utils/storage';

let rawIsInFullscreen = false;

const VideoQualityList = forwardRef(
  ({ reactPlayerRef, isPrerequisite }, ref) => {
    const [isQualityListOpen, setIsQualityListOpen] = useState(false);
    const [availableQualities, setAvailableQualities] = useState(null);
    const [currentQuality, setCurrentQuality] = useState('AUTO');

    useImperativeHandle(ref, () => ({
      closeQualitySettingMenu: () => {
        setIsQualityListOpen(false);
      },
    }));

    useEffect(() => {
      if (isQualityListOpen) {
        populateQualityList();
      }
    }, [isQualityListOpen]);

    const populateQualityList = () => {
      try {
        if (!isIOS) {
          let qList =
            reactPlayerRef.current
              .getInternalPlayer('dash')
              .getBitrateInfoListFor('video') || [];
          setAvailableQualities(qList);

          let lastItem = qList[qList.length - 1];
          console.log('----------------- tmp -->', lastItem);
          lastItem.height = 'HD';
          qList[qList.length - 1] = lastItem;

          qList.push({ height: 'AUTO', bitrate: 'auto' });
          console.log('DASH|HLS', qList);
        }
        // else if (isPrerequisite) {
        //   let qList = reactPlayerRef.current
        //     .getInternalPlayer()
        //     .getAvailablePlaybackRates();
        //   setAvailableQualities(qList);
        //   console.log('YT', qList);
        //   // console.log('youtube player -->', tmp);
        //   // let ret = [];

        //   // for (let i = 0; i < tmp.length; i++) {
        //   //   ret[i] = { height: tmp[i] * 1000 };
        //   // }
        //   // return ret;
        // }
      } catch (error) {
        console.log('getResolutionList::error -->', error);
      }
    };

    const changeQuality = ({ height, width, bitrate }) => {
      setCurrentQuality(height);
      if (!isIOS) {
        if (height === 'AUTO') {
          reactPlayerRef.current.getInternalPlayer('dash').updateSettings({
            streaming: {
              abr: {
                autoSwitchBitrate: {
                  video: true,
                },
              },
            },
          });
        } else {
          reactPlayerRef.current.getInternalPlayer('dash').updateSettings({
            streaming: {
              abr: {
                autoSwitchBitrate: {
                  video: false,
                },
              },
            },
          });

          reactPlayerRef.current.getInternalPlayer('dash').setQualityFor(
            'video',
            availableQualities.findIndex((a) => a.height === height)
          );
        }
      }
      setIsQualityListOpen(false);
    };

    return (
      <div>
        <img
          className={'w-10 h-10 relative mr-4'}
          alt={'settings'}
          src={settingsIcon}
          onClick={() => setIsQualityListOpen(!isQualityListOpen)}
        />
        {isQualityListOpen && (
          <div
            className={`absolute bottom-16  flex flex-col bg-white w-28 h-40 z-10  overflow-y-scroll`}
          >
            {availableQualities?.map((a) => (
              <div
                key={a.bitrate}
                onClick={() => changeQuality(a)}
                className={`cursor-pointer sans-book hover:bg-gray-100 px-4 py-2 text-lg  ${currentQuality === a.height ? ' bg-gray-300' : ' bg-white'
                  }`}
              >
                {a?.height}
              </div>
            ))}
          </div>
        )}
      </div>
    );
  }
);

let wdogRetryCnt = 0;
let wdogStartTime = 0;
let playerStartTime = 0;
let playerControlsVanishInterval = null;
let rawIsUsingFallback = false;
let rawShowStreamSwitchButton = false;
let wdogTimer = 0;

const PlayerTest = ({
  hlsURL,
  dashURL,
  youtubeURL,
  isPrerequisite,
  sessionId,
  type,
}) => {
  console.log(hlsURL, dashURL, youtubeURL, isPrerequisite);
  const WDOG_BUFFER_LIMIT = 240;
  const videoContainer = useRef(null);
  const iOSVideoRef = useRef(null);
  const reactPlayerRef = useRef(null);
  const qualitySettingsRef = useRef(null);
  // let tmp = isPrerequisite ? youtubeURL : isIOS ? hlsURL : dashURL;
  // const [currentVideoSource, setCurrentVideoSource] = useState(tmp);
  const [isUsingFallbackStream, setIsUsingFallbackStream] = useState(false);
  const [isInFullScreen, setIsInFullScreen] = useState(false);
  const [reactPlayerVolume, setReactPlayerVolume] = useState(1.0);
  const [lastReactPlayerVolume, setLastReactPlayerVolume] = useState(1.0);
  const [didVideoStart, setDidVideoStart] = useState(false);
  const [isBuffering, setIsBuffering] = useState(false);
  const [showStreamSwitchButton, setShowStreamSwitchButton] = useState(false);
  const [showPlayerAlert, setShowPlayerAlert] = useState(false);

  const [isPlaying, setIsPlaying] = useState(false);
  // const [wdogTimer, setWdogTimer] = useState(null);

  useEffect(() => {
    initCleverTap();
    initCleverTapUser(getUserId());

    document.addEventListener('fullscreenchange', (event) => {
      if (document.fullscreenElement) {
        console.log(
          `Element: ${document.fullscreenElement.id} entered full-screen mode.`
        );
      } else {
        console.log('Leaving full-screen mode.', rawIsInFullscreen);
        if (rawIsInFullscreen) {
          setIsInFullScreen(false);
          rawIsInFullscreen = false;
        }
      }
    });
  }, []);

  useEffect(() => {
    rawIsUsingFallback = isUsingFallbackStream;
  }, [isUsingFallbackStream]);
  useEffect(() => {
    rawShowStreamSwitchButton = showStreamSwitchButton;
  }, [showStreamSwitchButton]);

  const toggleFullScreen = () => {
    if (!isInFullScreen) {
      if (isIOS) {
        // if (iOSVideoRef.current.requestFullscreen) {
        //   iOSVideoRef.current.requestFullscreen();
        // } else if (iOSVideoRef.current.webkitRequestFullscreen) {
        //   /* Safari */
        //   iOSVideoRef.current.webkitRequestFullscreen();
        // } else if (iOSVideoRef.current.msRequestFullscreen) {
        //   /* IE11 */
        //   iOSVideoRef.current.msRequestFullscreen();
        // }

        // console.log( reactPlayerRef.current.getInternalPlayer())
        // reactPlayerRef.current.getInternalPlayer().requestFullScreen();

        iOSVideoRef.current?.requestFullScreen();

        return;
      }
      videoContainer.current.requestFullscreen();
      if (isMobile) {
        window.screen.orientation.lock('landscape');
      }
    } else {
      document.exitFullscreen();
      if (isMobile) {
        window.screen.orientation.lock('potrait');
      }
    }
    setIsInFullScreen((prev) => !prev);
    rawIsInFullscreen = !rawIsInFullscreen;
  };

  const getVideoDims = () => {
    if (isInFullScreen) {
      return { width: '100vw', height: '100vh' };
    } else {
      return isMobile
        ? { width: '100vw', height: 'calc(0.562*100vw)' }
        : { width: 720, height: 405 };
    }
  };

  const resetWdog = () => {
    console.log('resetWdog');
    wdogRetryCnt = 0;
    console.log(iOSVideoRef);
    let startTime = isIOS
      ? iOSVideoRef.current.currentTime
      : reactPlayerRef.current.getCurrentTime();
    playerStartTime = startTime;
    wdogStartTime = Date.now();
  };

  const watchdog = () => {
    try {
      let tmp = Date.now();
      let deltaWdog = Math.floor((tmp - wdogStartTime) / 1000);

      let currentPlayerTime = isIOS
        ? iOSVideoRef.current.currentTime
        : reactPlayerRef.current?.getCurrentTime?.() ?? -1;
      if (currentPlayerTime < 0) {
        return;
      }
      let deltaPlayer = currentPlayerTime - playerStartTime;

      let diff = deltaWdog - deltaPlayer;
      // let diff = Math.abs(deltaWdog - deltaPlayer);
      // console.log('watchdog::diff -->', diff);

      if (diff > WDOG_BUFFER_LIMIT) {
        if (diff < WDOG_BUFFER_LIMIT + 60) {
          //######################## Clever Tap debug
          recordCleverTapEvent('onWatchdog', {
            isIOS: isIOS,
            diffAbs: diff,
            diff: deltaWdog - deltaPlayer,
            currentPlayerTime: currentPlayerTime,
            playerStartTime: playerStartTime,
            wdogStartTime: wdogStartTime,
            wdogCurrentTime: tmp
          });
          //########################

          //when buffered for too long we show modal
          wdogRetryCnt++;

          if (!window.navigator.onLine) {
            //if network is gone we ask user to check internet and refresh
            //setShowModal("network");
            // toast('Network Not Available');
            //TODO: @Sunil:: open an popup telling user that network disappeared, there will be a button to
            //                refresh the page. If internet comes back while the popup is open it should automatically
            //               close and continue showing the video. This pop up should come when wathdog detected
            //               and internet connection is offline.
            //  <>
            //   <div className="h-8  font-bold text-sm sm:text-2xl ">Network Not Available</div>
            //   <div className="mb-2 text-sm  sm:text-lg text-center">Please check your internet and <br></br>reload the player</div>
            //   <SecondaryButton style={{ margin: "5px 5px" }} text="Reload Player" onClick={(evt) => { window.location.reload() }} />
            // </>
            // window.alert('Watchdog buffer event and internet is offline');
          } else {
            //TODO: @Sunil: open a popup telling the user that switch button has become available.

            !rawShowStreamSwitchButton && setShowStreamSwitchButton(true);
            !rawIsUsingFallback && setShowPlayerAlert(true);

            //              this message will only appear once after reload of the browser.
            //
            //             <>
            //   <div className="h-8 text-520-orange font-bold text-xl sm:text-2xl p">Backup video enabled</div>
            //   <div className="text-justify text-sm text-8ee-beige font-bold xs:text-lg text-center pt-0 pl-2 pr-2">
            //     If you experience buffering or low quality video
            //     you can switch to the backup video by clicking
            // <img className="inline pl-2 pr-2" src={BackupStreamBtn} />
            // in the menu.

            // Do you want to switch now?

            // </div>
            //   <div className="ml-auto mr-auto">
            //     <SecondaryButton
            //       style={{ margin: "10px 5px", width: "4rem", height: "3rem" }}
            //       text="Yes"
            //       onClick={switchStream}
            //     />
            //     <SecondaryButton
            //       style={{ margin: "10px 5px", width: "4rem", height: "3rem" }}
            //       text="No"
            //       onClick={closeSwitchStream}
            //     />
            //   </div>
            // </>

            // window.alert('Watchdog buffer event, show switch button');
          }
        }

        resetWdog();
      } 
      
      if (diff < -100){
        resetWdog();
      }
    } catch (error) {
      console.log('watchdog2::error -->', error);
    }
  };

  const onPlay = () => {
    try {
      console.log('onPlay::called');
      !didVideoStart && setDidVideoStart(true);
      //TODO: we need to enable cleverTap  defined in utils/cleverTap"
      // //######################## Clever Tap debug
      // recordCleverTapEvent('onPlay');
      // //########################
      if (isPrerequisite || isUsingFallbackStream) {
        return;
      }
      if (wdogTimer) {
        clearInterval(wdogTimer);
      }

      resetWdog();
      wdogTimer = setInterval(watchdog, 10000);
    } catch (error) {
      console.log('onPlay::error -->', error);
    }
  };

  const onReactPlayerReady = () => {
    //TODO: @Sunil, close spinner when player is ready
    setIsBuffering(false);

    //If its a dash video we are playing we need to change the player configuration
    if (!isIOS) {
      let dashPlayer = reactPlayerRef.current.getInternalPlayer('dash');
      dashPlayer?.updateSettings(dashConfig);
      console.log('found dash player ? --->', dashPlayer);
    }

    console.log('PLAYER LOADED');
    //settings for dash
    if (isIOS) {
      console.log('HLS SETTINGS');
    }
    //for youtube
    else if (isPrerequisite) {
      reactPlayerRef.current.getInternalPlayer();
    } else {
      if (isMobile) {
        //Only change max bitrate on mobile phone to avoid that they download high bandwidth stream
        reactPlayerRef.current.getInternalPlayer('dash').updateSettings({
          streaming: {
            abr: {
              maxBitrate: { audio: 1000, video: 1800 }, //restrict video bandwidth to 1
            },
          },
        });
      }
    }
  };

  const onBuffer = () => {
    //TODO: @Sunil, show spinner when buffering
    setIsBuffering(true);
    //TODO: we need to enable cleverTap  defined in utils/cleverTap"
    //######################## Clever Tap debug
    recordCleverTapEvent('onBuffer');
    //########################
  };

  const onBufferEnd = () => {
    //TODO: @Sunil, close spinner when buffering ended
    setIsBuffering(false);
    //######################## Clever Tap debug
    recordCleverTapEvent('onBufferEnd');
    //########################
  };

  const toggleVideo = () => {
    if (isIOS) {
      if (isPlaying) {
        iOSVideoRef.current?.pause();
      } else {
        iOSVideoRef.current?.play();
      }
    }

    setIsPlaying((prev) => !prev);
  };

  const toggleMute = (ev) => {
    if (isPlaying) {
      if (reactPlayerVolume <= 0.0) {
        //is muted
        setReactPlayerVolume(parseFloat(lastReactPlayerVolume));
      } else {
        setLastReactPlayerVolume(reactPlayerVolume);
        setReactPlayerVolume(0.0);
      }
    }
  };

  const onVolumeChange = (ev) => {
    if (isIOS) {
      iOSVideoRef.current.volume = parseFloat(ev.target.value);
    } else {
      setReactPlayerVolume(parseFloat(ev.target.value));
    }
  };

  const onMouseEnterVideoContainer = () => {
    clearTimeout(playerControlsVanishInterval);
    if (!didVideoStart && !isMobile) {
      return;
    }

    const target = document.getElementById('video-overlay-controls');

    target && target.classList.remove('invisible');

    playerControlsVanishInterval = setTimeout(hideControls, 3000);
  };

  const onMouseExitVideoContainer = () => {
    if (!didVideoStart && !isMobile) {
      return;
    }
    const target = document.getElementById('video-overlay-controls');
    target && target.classList.add('invisible');
    qualitySettingsRef.current?.closeQualitySettingMenu();
  };

  const hideControls = () => {
    const target = document.getElementById('video-overlay-controls');
    target && target.classList.add('invisible');
    qualitySettingsRef.current?.closeQualitySettingMenu();
  };

  const onTouchVideoContainer = () => {
    clearTimeout(playerControlsVanishInterval);
    // if (!didVideoStart) {
    if (!isMobile) {
      return;
    }

    if (!didVideoStart) {
      return;
    }

    const target = document.getElementById('video-overlay-controls');
    !isPrerequisite &&
      !isBuffering &&
      target &&
      target.classList.remove('invisible');

    playerControlsVanishInterval = setTimeout(hideControls, 3000);
  };

  const switchToYoutube = () => {
    setIsUsingFallbackStream(true);
    setShowPlayerAlert(false);
  };

  const toggleFallbackStream = () => {
    setIsUsingFallbackStream((prev) => !prev);
    //######################## Clever Tap debug
    recordCleverTapEvent('onFallback', {
      isIOS: isIOS,
      isUsingFallback: isUsingFallbackStream,
    });
    //########################
  };

  return (
    <div className={'flex flex-col flex-1 items-center justify-center mt-8'}>
      <div
        ref={videoContainer}
        className={'relative z-10'}
        onClick={onTouchVideoContainer}
        onMouseEnter={onMouseEnterVideoContainer}
        onMouseLeave={onMouseExitVideoContainer}
        onMouseMove={onMouseEnterVideoContainer}
      // onWheel={onMouseEnterVideoContainer}
      >
        {isIOS && !isPrerequisite && !isUsingFallbackStream ? (
          <video
            style={{ ...getVideoDims() }}
            id={'ios-player'}
            ref={iOSVideoRef}
            controls={true}
            src={hlsURL}
            onWaiting={onBuffer}
            onPlaying={onBufferEnd}
            onPlay={onPlay}
          />
        ) : (
            <div
              onClick={qualitySettingsRef.current?.closeQualitySettingMenu}
            // className="pointer-events-none"
            >
              {(isUsingFallbackStream || isPrerequisite) && (
                <YouTube
                  videoId={extractYoutubeId(youtubeURL)}
                  opts={{
                    width: isMobile ? window.innerWidth : 720,
                    height: isMobile ? window.innerWidth * 0.562 : 405,
                  }}
                  onEnd={() => {
                    logActivity(getUserId(), sessionId, type);
                    markSessionComplete(sessionId);
                  }}
                // onReady={this._onReady}
                />
              )}
              {!isIOS && !isPrerequisite && !isUsingFallbackStream && (
                <ReactPlayer
                  url={dashURL}
                  playing={isPlaying}
                  {...getVideoDims()}
                  onReady={onReactPlayerReady}
                  onPlay={onPlay}
                  controls={isIOS && (isPrerequisite || isUsingFallbackStream)}
                  onBuffer={onBuffer}
                  onBufferEnd={onBufferEnd}
                  ref={reactPlayerRef}
                  volume={reactPlayerVolume}
                // onStart={() => setDidVideoStart(true)}
                />
              )}
            </div>
          )}

        {!isIOS && !isPrerequisite && !isUsingFallbackStream && (
          <div className={'z-10'}>
            {!isPlaying && (
              <div
                onClick={toggleVideo}
                className="z-30 cursor-pointer absolute bottom-0 w-full h-full bg-gray-500 bg-opacity-60 flex flex-row justify-center items-center"
              >
                <img alt={'play-icon'} src={playIcon} className={'w-20 h-20'} />
              </div>
            )}
            {isBuffering && !isPrerequisite && !isUsingFallbackStream && (
              <div className="z-30 flex flex-row absolute bottom-0 w-full h-full items-center justify-center">
                <Spinner />
              </div>
            )}

            {
              <div
                id={'video-overlay-controls'}
                className="absolute invisible px-4 bottom-0 w-full justify-between h-20 bg-gray-500 bg-opacity-60 flex flex-row items-center"
              >
                <div className="flex flex-row items-center ">
                  <img
                    alt="vol-icon"
                    src={reactPlayerVolume <= 0 ? muteIcon : volumeIcon}
                    className={'w-10 h-10 mr-2'}
                    onClick={toggleMute}
                  />
                  <input
                    className="slider flex w-28"
                    type="range"
                    min={0}
                    max={1}
                    step={0.05}
                    value={reactPlayerVolume}
                    onChange={onVolumeChange}
                  />
                </div>
                <div className={'flex flex-row items-center'}>
                  {showStreamSwitchButton && (
                    <img
                      alt={'sync-icon'}
                      src={syncIcon}
                      onClick={toggleFallbackStream}
                      className={'w-10 h-10 mr-4'}
                    />
                  )}
                  {!isIOS && !isPrerequisite && !isUsingFallbackStream && (
                    <VideoQualityList
                      isPrerequisite={isPrerequisite}
                      reactPlayerRef={reactPlayerRef}
                      ref={qualitySettingsRef}
                    />
                  )}
                  <img
                    alt={'fscrn'}
                    src={fullScreenIcon}
                    onClick={toggleFullScreen}
                    className={'w-10 h-10 '}
                  />
                </div>
              </div>
            }
          </div>
        )}
      </div>

      {showPlayerAlert && (
        <div className={'text-xl mt-8 text-black px-4'}>
          Having video issues? Try switching to{' '}
          <span
            className={'underline cursor-pointer text-520-orange'}
            onClick={toggleFallbackStream}
          // onClick={switchToYoutube}
          >
            {' '}
            backup stream{' '}
          </span>
        </div>
      )}
    </div>
  );
};

export default PlayerTest;
