
const config = {
    'debug': {
        // 'logLevel': dashjs.Debug.LOG_LEVEL_INFO
    },
    'streaming': {
        fastSwitchEnabled: true,
        stableBufferTime: 20,
        minimumTimeBetweenSyncAttempts: 10,
        timeBetweenSyncAttemptsAdjustmentFactor: 5,
        retryAttempts: {
            MPD: 6,
            XLinkExpansion: 6,
            InitializationSegment: 6,
            IndexSegment: 6,
            MediaSegment: 6,
            BitstreamSwitchingSegment: 6,
            other: 6,
            lowLatencyMultiplyFactor: 2,
            jumpGaps: true,
            smallGapLimit: 1.5,
        },
    }
}


export default config; 