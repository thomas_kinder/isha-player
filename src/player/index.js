import React, {
    forwardRef,
    useEffect,
    useImperativeHandle,
    useRef,
    useState,
} from 'react';

import { isIOS, isMobile } from 'react-device-detect';
import ReactPlayer from 'react-player';
import fullScreenIcon from './assets/fullscreen.svg';
import playIcon from './assets/play_circle.svg';
import pauseIcon from './assets/pause.svg';
import settingsIcon from './assets/settings.svg';
import syncIcon from './assets/sync.svg';
import muteIcon from './assets/volume_off.svg';
import volumeIcon from './assets/volume_up.svg';
import Spinner from './Spinner';

let playbackUrl;
let playerControlsVanishInterval = null;
let bufferingSum = 0;
let bufferingStarted;
let bufferTimeout;
let resetCnt = 0;

let rawIsInFullscreen = false;
let rawGuiState = {};


const VideoQualityList = forwardRef(
    ({ reactPlayerRef, isPrerequisite }, ref) => {
        const [isQualityListOpen, setIsQualityListOpen] = useState(false);
        const [availableQualities, setAvailableQualities] = useState(null);
        const [currentQuality, setCurrentQuality] = useState('AUTO');

        useImperativeHandle(ref, () => ({
            closeQualitySettingMenu: () => {
                setIsQualityListOpen(false);
            },
        }));

        useEffect(() => {
            if (isQualityListOpen) {
                populateQualityList();
            }
        }, [isQualityListOpen]);

        const populateQualityList = () => {
            try {
                if (!isIOS) {
                    let qList =
                        reactPlayerRef.current
                            .getInternalPlayer('dash')
                            .getBitrateInfoListFor('video') || [];
                    setAvailableQualities(qList);

                    let lastItem = qList[qList.length - 1];
                    lastItem.height = 'HD';
                    qList[qList.length - 1] = lastItem;

                    qList.push({ height: 'AUTO', bitrate: 'auto' });

                }
                // else if (isPrerequisite) {
                //   let qList = reactPlayerRef.current
                //     .getInternalPlayer()
                //     .getAvailablePlaybackRates();
                //   setAvailableQualities(qList);
                //   console.log('YT', qList);
                //   // console.log('youtube player -->', tmp);
                //   // let ret = [];

                //   // for (let i = 0; i < tmp.length; i++) {
                //   //   ret[i] = { height: tmp[i] * 1000 };
                //   // }
                //   // return ret;
                // }
            } catch (error) {
                console.log('getResolutionList::error -->', error);
            }
        };

        const changeQuality = ({ height, width, bitrate }) => {
            setCurrentQuality(height);
            if (!isIOS) {
                if (height === 'AUTO') {
                    reactPlayerRef.current.getInternalPlayer('dash').updateSettings({
                        streaming: {
                            abr: {
                                autoSwitchBitrate: {
                                    video: true,
                                },
                            },
                        },
                    });
                } else {
                    reactPlayerRef.current.getInternalPlayer('dash').updateSettings({
                        streaming: {
                            abr: {
                                autoSwitchBitrate: {
                                    video: false,
                                },
                            },
                        },
                    });

                    reactPlayerRef.current.getInternalPlayer('dash').setQualityFor(
                        'video',
                        availableQualities.findIndex((a) => a.height === height)
                    );
                }
            }
            setIsQualityListOpen(false);
        };

        return (
            <div>
                <img
                    className={'w-12 h-12 relative mr-4'}
                    alt={'settings'}
                    src={settingsIcon}
                    onClick={() => setIsQualityListOpen(!isQualityListOpen)}
                />
                {isQualityListOpen && (
                    <div
                        className={`absolute bottom-16  flex flex-col bg-white w-28 h-40 z-10  overflow-y-scroll`}
                    >
                        {availableQualities?.map((a) => (
                            <div
                                key={a.bitrate}
                                onClick={() => changeQuality(a)}
                                className={`cursor-pointer sans-book hover:bg-gray-100 px-4 py-2 text-lg  ${currentQuality === a.height ? ' bg-gray-300' : ' bg-white'
                                    }`}
                            >
                                {a?.height}
                            </div>
                        ))}
                    </div>
                )}
            </div>
        );
    }
);



export const IshaPlayer = ({
    hlsURL,
    dashURL,
    youtubeURL,
    onEnded, //callback executed when playback finished
    playerSelect, // "yt" for youtube videos, "stream" --> streaming with dash or hls or youtube fallback
    overlayMenu = true,
    volumeControls = true,
    streamSwitchControl = false,
    playControls = false,
    timeLineControl = false,
}) => {

    const [isUsingFallbackStream, setIsUsingFallbackStream] = useState(false);
    const [isInFullScreen, setIsInFullScreen] = useState(false);
    const [reactPlayerVolume, setReactPlayerVolume] = useState(1.0);
    const [lastReactPlayerVolume, setLastReactPlayerVolume] = useState(1.0);

    // const [isBuffering, setIsBuffering] = useState(false); //indicates if player is buffering
    const [selectedPlayer, setSelectedPlayer] = useState(null);
    const [playerOverlay, setPlayerOverlay] = useState(null);

    const [guiState, setGuiState] = useState({
        overlaySpinner: false,
        overlayPlay: true,
        volumeControls: volumeControls,
        streamSwitch: streamSwitchControl,
        isPlaying: false,
        isMuted: true,
        ytInitialized: false,
        playControls: playControls,
        timeLineControl: timeLineControl,
        played: 0,
        playedSeconds: 0,
    });

    const videoContainerRef = useRef(null);
    const iOSVideoRef = useRef(null);
    const reactPlayerRef = useRef(null);
    const qualitySettingsRef = useRef(null);

    const hideControls = () => {
        const target = document.getElementById('video-overlay-controls');
        target && target.classList.add('invisible');
        qualitySettingsRef.current?.closeQualitySettingMenu();
    };

    const getVideoDims = () => {
        if (isInFullScreen) {
            return { width: '100vw', height: '100vh' };
        } else {

            return isMobile
                ? { width: '100vw', height: 'calc(0.562*100vw)' }
                : { width: 720, height: 'calc(0.562 * 720px)' };
        }
    };

    const onPlay = () => {
        let tmp = { ...guiState };

        if (playbackUrl === youtubeURL && !tmp.ytInitialized) { //youtube playback, so pause, wait 5 seconds and start again

            tmp.ytInitialized = true;

            setTimeout(() => {
                let tmp = { ...rawGuiState };
                tmp.overlaySpinner = false;
                tmp.overlayPlay = false;
                tmp.isPlaying = true;
                tmp.isMuted = false;
                tmp.ytInitialized = true;

                reactPlayerRef.current.seekTo(0);
                setGuiState(tmp);
            }, 5000)

        } else {
            tmp.overlaySpinner = false;
            tmp.overlayPlay = false;
        }

        setGuiState(tmp);
        clearInterval(playerControlsVanishInterval);
        playerControlsVanishInterval = setTimeout(hideControls, 3000);
    }


    const onError = (error) => {
        const RST_CNT = 10; //change this value to define how many error will lead to show the switch stream button

        if (resetCnt <= RST_CNT) {
            setTimeout(() => {
                resetCnt++;

                let tmp = { ...guiState }
                tmp.reset = true;

                if (resetCnt > RST_CNT) {
                    tmp.showError = true;
                    tmp.streamSwitch = true;

                    let errToast = document.getElementById('toast-error');
                    errToast.classList.remove('invisible');
                }

                setGuiState(tmp);

            }, 3000);
        }

    }

    const onProgress = (data) => {
        let tmp = { ...guiState };
        tmp.playedSeconds = data.playedSeconds;

        let item = document.getElementById('isha-video-progress-bg');
        let rect = item.getBoundingClientRect();

        tmp.played = rect.width * data.played;
        setGuiState(tmp);
    }

    const toggleVideo = () => {
        let tmp = { ...guiState };

        if (isIOS) {
            if (guiState.isPlaying) {
                iOSVideoRef.current?.pause();
                tmp.isPlaying = false;
            } else {
                iOSVideoRef.current?.play();
                tmp.isPlaying = true;
            }
        } else {

            if (!guiState.isPlaying) {
                tmp.overlayPlay = false;
                tmp.overlaySpinner = false;
                tmp.isPlaying = true;
                tmp.isMuted = false;

                if (playbackUrl === youtubeURL && !guiState.ytInitialized) { //youtube playback
                    tmp.overlaySpinner = true;
                    tmp.isMuted = true;
                }

                setGuiState(tmp)

            } else {
                if (playbackUrl === youtubeURL && !guiState.ytInitialized) { //do not allow play/pause toggle when yt is not yet initialized
                    return;
                }

                tmp.overlayPlay = false;
                tmp.overlaySpinner = false;
                tmp.isPlaying = false;
                tmp.overlaySpinner = false;
                tmp.isMuted = false;
                setGuiState(tmp)
            }

        }
    };

    const toggleMute = (ev) => {
        if (guiState.isPlaying) {
            if (reactPlayerVolume <= 0.0) { //is muted
                setReactPlayerVolume(parseFloat(lastReactPlayerVolume));
            } else {
                setLastReactPlayerVolume(reactPlayerVolume);
                setReactPlayerVolume(0.0);
            }
        }
    };

    const onVolumeChange = (ev) => {
        if (isIOS) {
            iOSVideoRef.current.volume = parseFloat(ev.target.value);
        } else {
            setReactPlayerVolume(parseFloat(ev.target.value));
        }

        setLastReactPlayerVolume(reactPlayerVolume);
    };

    const toggleFallbackStream = () => {

        let tmp = { ...guiState };
        tmp.ytInitialized = false;

        if (!isUsingFallbackStream) {//if fallback stream is used, meaning youtube set up everything for hidden transition
            tmp.isMuted = true;
            tmp.overlaySpinner = true;
        }


        setGuiState(tmp);

        setIsUsingFallbackStream((prev) => !prev);
    };

    const toggleFullScreen = () => {
        if (!isInFullScreen) {
            if (isIOS) {
                iOSVideoRef.current?.requestFullScreen();
                return;
            }

            videoContainerRef.current.requestFullscreen();
            // setYoutubeStyle({ ...getVideoDims() })

            if (isMobile) {
                window.screen.orientation.lock('landscape');
            }

        } else {
            document.exitFullscreen();
            if (isMobile) {
                window.screen.orientation.lock('potrait'); //TODO: is potrait correct? or should it be portrait?
            }
        }
        setIsInFullScreen((prev) => !prev);
        rawIsInFullscreen = !rawIsInFullscreen;
    };



    const onTouchVideoContainer = () => {
        clearTimeout(playerControlsVanishInterval);

        //Reset Error Toast
        if (guiState.showError) {
            let tmp = { ...guiState };
            tmp.showError = false;
            let errToast = document.getElementById('toast-error');
            errToast?.classList.add('invisible');
            setGuiState(tmp);
        }

        if (!isMobile) {
            return;
        }

        const target = document.getElementById('video-overlay-controls');

        if (overlayMenu) {
            target?.classList.remove('invisible');
        }

        playerControlsVanishInterval = setTimeout(hideControls, 3000);
    };

    const onMouseEnterVideoContainer = () => {
        if (!overlayMenu) {
            return;
        }
        clearTimeout(playerControlsVanishInterval);
        const target = document.getElementById('video-overlay-controls');
        target && target.classList.remove('invisible');
        playerControlsVanishInterval = setTimeout(hideControls, 3000);
    };

    const onMouseExitVideoContainer = () => {
        hideControls();
    };

    const onBuffer = () => {

        if (!guiState.streamSwitch && playerSelect === "stream") {//only check this is switching stream is not already active

            bufferingStarted = Date.now();

            bufferTimeout = setTimeout(() => {
                let tmp = { ...rawGuiState };
                tmp.streamSwitch = true;
                alert("continious buffering for more thatn 30 seconds")
                setGuiState(tmp);
            }, 30000);

        } else {
            clearTimeout(bufferTimeout);
        }

        let tmp = { ...guiState };
        tmp.overlayLoading = true;
        setGuiState(tmp);
    }

    const onBufferEnd = () => {
        let tmp = { ...rawGuiState };

        if (!guiState.streamSwitch && playerSelect === "stream") {//only check this is switching stream is not already active
            let delta = (Date.now() - bufferingStarted);

            if (delta) {
                bufferingSum = bufferingSum + delta;
            }

            clearTimeout(bufferTimeout);

            if (bufferingSum > 4 * 60 * 1000) {//greater 4 minutes
                tmp.streamSwitch = true;
            }
        }

        tmp.overlayLoading = false;
        setGuiState(tmp);
    }

    const onMoveTimeline = (data) => {
        if (guiState.isPlaying) {
            let item = document.getElementById('isha-video-progress-bg');
            let rect = item.getBoundingClientRect();

            let newPosition = (data.pageX - rect.x) / rect.width
            let duration = reactPlayerRef.current.getDuration();
            reactPlayerRef.current.seekTo(newPosition * duration);

            let tmp = { ...guiState };
            tmp.played = (data.pageX - rect.x);
            setGuiState(tmp);
        }
    }

    const getPlayer = () => {
        if (isIOS && !isUsingFallbackStream) { //For Iphone play on native player with HLS video
            return (
                <video
                    style={{ ...getVideoDims() }}
                    id={'ios-player'}
                    ref={iOSVideoRef}
                    controls={true}
                    src={hlsURL}
                    onWaiting={onBuffer}
                    onPlaying={onBufferEnd}
                    onPlay={onPlay}

                />
            )
        } else { //Use the youtube player when IOS and fallback, or playerSelect is set to yt

            if (guiState.reset) {
                let tmp = { ...guiState }
                tmp.reset = false;
                setGuiState(tmp);

                bufferingSum = 0;
                return null;
            }


            playbackUrl = (playerSelect === "yt" || isUsingFallbackStream) ? youtubeURL : dashURL;
            let tmpClass = (playbackUrl === youtubeURL && overlayMenu) ? "pointer-events-none" : "";

            return (
                <ReactPlayer
                    ref={reactPlayerRef}
                    className={tmpClass + " bg-gray-800"}
                    url={playbackUrl}
                    playing={guiState.isPlaying}
                    muted={guiState.isMuted}
                    volume={reactPlayerVolume}
                    onBuffer={onBuffer}
                    onBufferEnd={onBufferEnd}
                    onEnded={onEnded}
                    onPlay={onPlay}
                    onError={onError}
                    onProgress={onProgress}
                    {...getVideoDims()}
                />
            )
        }
    }

    const getPlayerOverlay = () => {
        if (isIOS) {
            return null;
        }

        const getOverlay = () => {
            let playerOverlayItem = [
                "z-10",
                "cursor-pointer",
                "absolute",
                "bottom-0",
                "w-full",
                "h-full",

                "flex",
                "flex-row",
                "justify-center",
                "items-center"
            ]

            if (guiState.overlayPlay) {
                playerOverlayItem.push("bg-gray-800");

                return (
                    <div onClick={toggleVideo} className={playerOverlayItem.join(" ")}>
                        <img alt={'play-icon'} src={playIcon} className={'w-20 h-20'} />
                    </div>
                );

            } else if (guiState.overlaySpinner) {
                playerOverlayItem.push("bg-gray-800");
                return (
                    <div className={playerOverlayItem.join(" ")}>
                        <Spinner />
                    </div>
                );
            } else if (guiState.overlayLoading) {
                return (
                    <div className={playerOverlayItem.join(" ")}>
                        <Spinner />
                    </div>
                );
            }
            return null;
        }

        const getMenu = () => {
            let controlsClass = [
                "absolute",
                "invisible",
                "px-4",
                "bottom-0",
                "w-full",
                "justify-between",
                "bg-gray-800",
                "bg-opacity-80",
                "flex",
                "flex-col",
                "items-center",
                "z-30"
            ]

            return (
                <div id={'video-overlay-controls'} className={controlsClass.join(" ")}>

                    <div className={"p-0 w-full h-2 mt-2" + (guiState.timeLineControl ? " " : " invisible")}>
                        <div
                            className="h-2 bg-blue-600 hover:bg-blue-400 absolute inline"
                            style={{
                                width: guiState.played.toString() + "px",
                            }}
                            onClick={onMoveTimeline}
                        >

                        </div>
                        <div
                            id="isha-video-progress-bg"
                            className="z-30 w-full h-2 bg-gray-600 hover:bg-gray-400"
                            onClick={onMoveTimeline}
                        >
                        </div>
                    </div>

                    <div className={"flex flex-row items-center w-full h-16"}>
                        <img
                            alt="play-icon"
                            src={guiState.isPlaying ? pauseIcon : playIcon}
                            className={'w-12 h-12 mr-2 cursor-pointer' + (guiState.playControls ? " " : " hidden")}
                            onClick={toggleVideo}
                        />


                        <div className={"flex flex-row items-center " + (guiState.volumeControls ? " " : " hidden")} >
                            <img
                                alt="vol-icon"
                                src={reactPlayerVolume <= 0 ? muteIcon : volumeIcon}
                                className={'w-12 h-12 mr-2 cursor-pointer'}
                                onClick={toggleMute}
                            />
                            <input
                                className="slider flex w-28 cursor-pointer"
                                type="range"
                                min={0}
                                max={1}
                                step={0.05}
                                value={reactPlayerVolume}
                                onChange={onVolumeChange}
                            />
                        </div>

                        <div className={" flex-grow bg-green-400 invisible"} > Filler</div>

                        <img
                            alt={'sync-icon'}
                            src={syncIcon}
                            onClick={toggleFallbackStream}
                            className={'w-12 h-12 mr-4 cursor-pointer' + (guiState.streamSwitch ? " " : " hidden")}
                        />



                        <div className={"flex flex-row items-end"}>
                            <div className={(playbackUrl !== youtubeURL ? " " : " hidden")}>

                                <VideoQualityList
                                    reactPlayerRef={reactPlayerRef}
                                    ref={qualitySettingsRef}
                                />
                            </div>


                            <img
                                alt={'fscrn'}
                                src={fullScreenIcon}
                                onClick={toggleFullScreen}
                                className={'w-12 h-12 cursor-pointer'}
                            />

                        </div >
                    </div >
                </div>
            );
        }

        const getToast = () => {
            let toastClass = [
                "z-10",
                "absolute",
                "top-0",
                "w-full",
                "flex",
                "flex-row",
                "justify-center",
                "items-center",
            ]


            return (
                <div className={toastClass.join(" ")}>
                    <div id="toast-error" className="invisible text-sm gap-0 text-justify border-red-500 border-2 w-full rounded text-white p-2 m-2 bg-red-900">

                        <div className="cursor-pointer -gap-4 flex-col justify-center items-center flex p-0 m-0">
                            An error has been detected. You can switch to the backup video in the menu.
                            <img alt={'play-icon'} src={syncIcon} className={'rounded border-2 hover:bg-gray-800 w-6 h-6'} />
                        </div>
                    </div>
                </div>

            );

        }


        return (
            <div className={'z-40 bg-gray-500'}>
                {getToast()}
                {getOverlay()}
                {getMenu()}
            </div>
        )

    }



    // const watchdog = () => {
    //     try {
    //         let tmp = Date.now();
    //         let deltaWdog = Math.floor((tmp - wdogStartTime) / 1000);

    //         let currentPlayerTime = isIOS
    //             ? iOSVideoRef.current.currentTime
    //             : reactPlayerRef.current?.getCurrentTime?.() ?? -1;

    //         if (currentPlayerTime < 0) {
    //             return;
    //         }

    //         let deltaPlayer = currentPlayerTime - playerStartTime;
    //         let diff = deltaWdog - deltaPlayer;

    //         if (diff > WDOG_BUFFER_LIMIT) {
    //             if (diff < WDOG_BUFFER_LIMIT + 60) {
    //                 //when buffered for too long we show modal
    //                 wdogRetryCnt++;

    //                 if (!window.navigator.onLine) {
    //                     //if network is gone we ask user to check internet and refresh
    //                     //setShowModal("network");
    //                     // toast('Network Not Available');
    //                     //TODO: @Sunil:: open an popup telling user that network disappeared, there will be a button to
    //                     //                refresh the page. If internet comes back while the popup is open it should automatically
    //                     //               close and continue showing the video. This pop up should come when wathdog detected
    //                     //               and internet connection is offline.
    //                     //  <>
    //                     //   <div className="h-8  font-bold text-sm sm:text-2xl ">Network Not Available</div>
    //                     //   <div className="mb-2 text-sm  sm:text-lg text-center">Please check your internet and <br></br>reload the player</div>
    //                     //   <SecondaryButton style={{ margin: "5px 5px" }} text="Reload Player" onClick={(evt) => { window.location.reload() }} />
    //                     // </>
    //                     // window.alert('Watchdog buffer event and internet is offline');
    //                 } else {

    //                     !rawShowStreamSwitchButton && setShowStreamSwitchButton(true);
    //                     !rawIsUsingFallback && setShowPlayerAlert(true);

    //                     //              this message will only appear once after reload of the browser.
    //                     //
    //                     //             <>
    //                     //   <div className="h-8 text-520-orange font-bold text-xl sm:text-2xl p">Backup video enabled</div>
    //                     //   <div className="text-justify text-sm text-8ee-beige font-bold xs:text-lg text-center pt-0 pl-2 pr-2">
    //                     //     If you experience buffering or low quality video
    //                     //     you can switch to the backup video by clicking
    //                     // <img className="inline pl-2 pr-2" src={BackupStreamBtn} />
    //                     // in the menu.

    //                     // Do you want to switch now?

    //                     // </div>
    //                     //   <div className="ml-auto mr-auto">
    //                     //     <SecondaryButton
    //                     //       style={{ margin: "10px 5px", width: "4rem", height: "3rem" }}
    //                     //       text="Yes"
    //                     //       onClick={switchStream}
    //                     //     />
    //                     //     <SecondaryButton
    //                     //       style={{ margin: "10px 5px", width: "4rem", height: "3rem" }}
    //                     //       text="No"
    //                     //       onClick={closeSwitchStream}
    //                     //     />
    //                     //   </div>
    //                     // </>

    //                     // window.alert('Watchdog buffer event, show switch button');
    //                 }
    //             }

    //             resetWdog();
    //         }

    //         if (diff < -100) {
    //             resetWdog();
    //         }
    //     } catch (error) {
    //         console.log('watchdog2::error -->', error);
    //     }
    // };

    useEffect(() => {
        setSelectedPlayer(getPlayer());
        setPlayerOverlay(getPlayerOverlay());

        document.addEventListener('fullscreenchange', (event) => {
            if (document.fullscreenElement) {

            } else {

                if (rawIsInFullscreen) {
                    setIsInFullScreen(false);
                    rawIsInFullscreen = false;
                }
            }
        });
    }, []);


    useEffect(() => {
        setSelectedPlayer(getPlayer());
        setPlayerOverlay(getPlayerOverlay());
        rawGuiState = guiState;

        //Reset buffer timeout if not playing and wait for the next buffering event.
        if (!guiState.isPlaying) {
            clearTimeout(bufferTimeout);
        }

        //Reset the Toast
        if (!guiState.showError) {
            let errToast = document.getElementById('toast-error');
            errToast?.classList.add('invisible');
        }

    }, [guiState, reactPlayerVolume, isInFullScreen, isUsingFallbackStream]);



    return (
        <div className={'flex flex-col flex-1 items-center justify-center mt-8'}>
            <div
                ref={videoContainerRef}
                className={'relative z-10'}
                onClick={onTouchVideoContainer}
                onMouseEnter={onMouseEnterVideoContainer}
                onMouseLeave={onMouseExitVideoContainer}
                onMouseMove={onMouseEnterVideoContainer}
            >
                {selectedPlayer}
                {playerOverlay}

            </div>
        </div >
    )
}
