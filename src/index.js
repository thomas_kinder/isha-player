import React from 'react';
import ReactDOM from 'react-dom';
import { IshaPlayer } from './player'
import { config } from './config'
import './index.css';
const logDebug = require('debug')('ishaPlayer::debug::');
const logError = require('debug')('ishaPlayer::error::');



export const ipRender = (id, config) => {
    try {
        ReactDOM.render(
            <IshaPlayer {...config} />,
            document.getElementById(id)
        );
    } catch (error) {
        logError('ipRender: render failed -->', error);
    }
}


if (config.useGlobalFunctions) {//if global variables are enabled set them up here
    global.ipRender = ipRender;
}

/*
    automatic render the player into testing page in development mode, 
    later we can disable for stand alone module apps
*/
if (config.mode === "dev") {
    let playerOptions = {
        hlsURL: "https://multiplatform-f.akamaihd.net/i/multi/will/bunny/big_buck_bunny_,640x360_400,640x360_700,640x360_1000,950x540_1500,.f4v.csmil/master.m3u8",
        dashURL: "https://cph-msl.akamaized.net/dash/live/2003285/test/manifest.mpd",
        youtubeURL: "https://www.youtube.com/watch?v=4pF3ofWbs6k",
        onEnded: () => {window.alert("video ended")},
        playerSelect: "stream",
        overlayMenu: true,
        streamSwitchControl: false,
        volumeControls: true,
        playControls: true,
    }
    ipRender('root', playerOptions);
} else {
    ipRender('ishaPlayer', window.playerOptions)
}





