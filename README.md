# Intro
This is the isha-player module which can be loaded as a component into 
other modules by sourcing the src/index.js file into ES6 module (basically for react)
or this project can be used to generate a java script file which can be 
integrated into PHP / HTML workflows and renders the media player into 
a specified div element. This is a media player mainly used for 
streaming application and not video on demand at the moment. 


# Features 
- can play youtube, HLS and Dash video streams 
- youtube overlay functionality 
- switching to backup stream if configured 


# Installation 
yarn 


# Build static binary
yarn build 

# Run dev server 
yarn start


# How to compile as standalone library for PHP - HTML applications
- change mode in config to **prod**
- ensure **useGlobalFunctions** is set to true in config

- then build the output files to include in php project
yarn build


- Add script to HTML
<script type="text/javascript" src="<path_to_binary>.js"></script> 

- Set the player configuration and render the video player into the div with id video0


```html
<script> 
    playerConfig = {
        hlsURL: "https://multiplatform-f.akamaihd.net/i/multi/will/bunny/big_buck_bunny_,640x360_400,640x360_700,640x360_1000,950x540_1500,.f4v.csmil/master.m3u8",
        dashURL: "https://cph-msl.akamaized.net/dash/live/2003285/test/manifest.mpd",           
        youtubeURL: "https://www.youtube.com/watch?v=4pF3ofWbs6k"
        onEnded: () =>  {window.alert("Video ended")}
        playerSelect: "yt", 
        overlayMenu: true,
        streamSwitchControl: false,
        volumeControls: true
    }
  
    window.ipRender('video0', playerConfig);

</script>

```

- copy the static directory from the output directory and include the 
main css and javascript files into the html project. 








